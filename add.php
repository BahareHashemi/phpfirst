<html>
<head><title>Addition php</title></head>
<body>

<?php
// This is a single-line comment

# This is also a single-line comment

/*
This is a multiple-lines comment block
that spans over multiple
lines
*/

  	# operator
	#Remember that PHP variable names are case-sensitive!
	#In PHP, all keywords (e.g. if, else, while, echo, etc.), classes, functions, and user-defined functions are NOT case-sensitive.
	print "<h2>php program to add two numbers...</h2><br />";
              $val1 = 1000;
              $val2 = 2990;
			  $val3 = 3000;
              $sum = $val2 + $val2 + $val3;   /* Assignment operator */
              echo "Result(SUM): $sum <br />";
?>
<?PHP
echo 'I know how to run a PHP Program in XAMPP! <br />';
?>

<?PHP
echo 'first in git <br />';
?>
<?php
$color = "red";
$COLOR = "blue";
echo "My car is " . $color . "<br>";
echo "My house is " . $COLOR . "<br>";
echo "My boat is " . $coLOR . "<br>";
echo "<br>";
?> 

<?php
function myTest() {
    static $x = 0;
    echo $x;
    $x++;
	echo "<br>";
}

myTest();
#echo "<br>";
myTest();
echo "<br>";
myTest();
echo "<br>";
?> 

<?php
#constant
define("GREETING", "Welcome to W3Schools.com!");

function my() {
    echo GREETING;
}
 
my();
echo "<br>";
?> 

<?php
#Concatenation assignment
$txt1 = "Hello";
$txt2 = " world!";
$txt1 .= $txt2;
echo $txt1;
echo "<br>";
?>  
<?php
#A function will not execute immediately when a page loads.

#A function will be executed by a call to the function.
#Function names are NOT case-sensitive.Function names are NOT case-sensitive.
function writeMsg() {
    echo "Hello world!12334455677";
}

writeMsg(); // call the function
echo "<br>";
?>
<?php
function familyName($fname, $year) {
    echo "$fname Refsnes. Born in $year <br>";
}

familyName("Hege", "1975");
familyName("Stale", "1978");
familyName("Kai Jim", "1983");
echo "<br>";
?>
<?php
function setHeight($minheight = 50) {
    echo "The height is : $minheight <br>";
}

setHeight(350);
setHeight(); // will use the default value of 50
setHeight(135);
setHeight(80);
echo "<br>";
?>
<?php
function sum($x, $y) {
    $z = $x + $y;
    return $z;
}

echo "5 + 10 = " . sum(5, 10) . "<br>";
echo "7 + 13 = " . sum(7, 13) . "<br>";
echo "2 + 4 = " . sum(2, 4);
echo "<br>";
?>

</body>
</html>